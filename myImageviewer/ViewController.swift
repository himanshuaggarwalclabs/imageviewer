//
//  ViewController.swift
//  myImageviewer
//
//  Created by Samar Singla on 14/01/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // TextField to get ImageNumber
    @IBOutlet weak var imgNumber: UITextField!
    
    // ImageViewer
    @IBOutlet weak var imgViewer: UIImageView!
    
    // Action of Pressed Button
    @IBAction func imgPressed(sender: AnyObject) {
       
        //self.view.endEditing(true)
        
        var number = imgNumber.text
        
        switch number {
            
        case "1":
            
            var image = UIImage(named:"img1.jpeg")
            
            imgViewer.image = image
            
        case "2":
            
            
            
            var image = UIImage(named:"img2.jpeg")
            
            imgViewer.image = image
            
        case "3":
            
            
            
            var image = UIImage(named:"img3.jpeg")
            
            imgViewer.image = image
            
        case "4":
            
            
            
            var image = UIImage(named:"img4.jpeg")
            
            imgViewer.image = image
            
        case "5":
            
            
            
            var image = UIImage(named:"img5.jpeg")
            
            imgViewer.image = image
            
        case "6":
            
            
            
            var image = UIImage(named:"img6.jpeg")
            
            imgViewer.image = image
            
        case "7":
            
            
            
            var image = UIImage(named:"img7.jpeg")
            
            
            
            imgViewer.image = image
            
            
            
        case "8":
            
            
            
            var image = UIImage(named:"img8.jpeg")
            
            imgViewer.image = image
            
        case "9":
            
            
            
            var image = UIImage(named:"img9.jpeg")
            
            imgViewer.image = image
            
        case "10":
            
            
            
            var image = UIImage(named:"ig10.jpeg")
            
            imgViewer.image = image
            
        default:
            
            var image = UIImage(named:"img1.jpeg")
            
            imgViewer.image = image
            
            
            
        }

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

